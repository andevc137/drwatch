from firefox import Firefox
from walmart import Walmart
from zalo import Zalo
from net import get_products, set_status
import time
from time import sleep

def check():
    products = get_products()

    firefox = Firefox(profile='Default', options={
        'init_folder': 'profiles',
        'headless': True,
        'executable_path': './geckodriver'
    })

    walmart = Walmart(firefox)
    timestamp = time.time()
    noti = []

    for product in products:
        print('product %s %s' % (product['price'], product['url']))

        if product['status'] == 'outstock':
            print('out of stock skip')
            continue

        url = product['url']
        id = product['_id']

        try:
            result = walmart.get_product_info(url)
            price = float(product['price'])
            if result['out_of_stock']:
                noti.append('Out of stock: %s' % url)
                set_status(id, 'outstock')

            if abs(result['price'] - price) >= 2:
                noti.append('Price change from $%s to $%s: %s' % (result['price'], price, url))
                set_status(id, 'changed')
        except:
            pass

        print("--- %s seconds ---" % (time.time() - timestamp))

    if len(noti) > 0:
        zalo = Zalo(firefox.get_browser())
        zalo.bulk_send_msg('Giapnguyen', noti)

    # input('press any key to exit....')
    firefox.quit_and_sync()

if __name__ == "__main__":
    while True:
        check()
        print('wait for next checking...')
        sleep(7200)
