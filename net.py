import requests
import os
import random

def get_products():
    url = 'http://54.177.52.107/api/products'
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}

    res = requests.get(url, headers=headers)
    json = res.json()

    if 'data' in json:
        return json['data']

    return None

def set_status(id, status):
    url = 'http://54.177.52.107/api/products/' + str(id)
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    payload = {'status': status}

    res = requests.put(url, headers=headers, json=payload)
