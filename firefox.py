from seleniumwire import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from base64 import b64encode
import os, shutil
from time import sleep

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            if os.path.exists(d):
                shutil.rmtree(d)
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def only_files_from(src):
    return [f for f in os.listdir(src) if os.path.isfile(os.path.join(src, f)) and not f.endswith('.lock')]

class Firefox:
    def __init__(self, profile='Default', options={}):
        self.profile = profile
        self.options = options
        self.profile_dir = None
        self.profile_folder_name = 'gecko_profiles'
        if 'init_folder' in options:
            self.profile_folder_name = options['init_folder']
        self.__create()

    def __init_profile(self,profile):
        dst = os.path.join('./%s' % self.profile_folder_name, profile)

        init_profile ='init'
        if 'init_profile' in self.options:
            init_profile = self.options['init_profile']

        src = os.path.join('./%s' % self.profile_folder_name, init_profile)

        if not os.path.exists(dst):
            print('create dst')
            os.makedirs(dst)
            print('copy profile')
            copytree(src, dst)
            print('done copy')
    
    def __sync_profile(self,src,profile):
        dst = os.path.join('./%s' % self.profile_folder_name, profile)

        if os.path.exists(src):
            print('sync profile')
            print('copy file')
            for item in only_files_from(src):
                s = os.path.join(src, item)
                d = os.path.join(dst, item)
                if os.path.exists(s):
                    shutil.copy2(s,d)

            print('copy folder')
            dirs = ['storage']
            for item in dirs:
                s = os.path.join(src, item)
                d = os.path.join(dst, item)
                copytree(s,d)

            print('sync done')

    def __create(self):
        options = Options()

        self.__init_profile(self.profile)

        print('Create firefox')
        profile = FirefoxProfile('./%s/%s' % (self.profile_folder_name, self.profile))
        # useragent
        if 'useragent' in self.options:
            profile.set_preference('general.useragent.override', self.options['useragent'])

        options.headless = True
        if 'headless' in self.options:
            options.headless = self.options['headless']

        options.profile = profile
        wire_options = {}

        if 'http_proxy' in self.options:
            wire_options['proxy'] = {
                'http': 'http://' + self.options['http_proxy'],
                'https': 'https://' + self.options['http_proxy'],
                'no_proxy': 'localhost,127.0.0.1'
            }

        executable_path = './geckodriver.exe'
        if 'executable_path' in self.options:
            executable_path = self.options['executable_path']

        self.driver = webdriver.Firefox(options=options,seleniumwire_options=wire_options,executable_path=executable_path)
        # self.driver.minimize_window()

    def get(self,url):
        try:
            self.driver.get(url)
        except:
            pass

    def get_browser(self):
        return self.driver

    def get_profile_folder(self):
        self.driver.get("about:support")
        sleep(0.1)
        profile_dir_btn = WebDriverWait(self.driver, 60).until(
            EC.presence_of_element_located((By.ID, 'profile-dir-box'))
        )
        self.profile_dir = profile_dir_btn.get_attribute('innerText').strip()

    def sync_data(self):
        if self.profile_dir is None:
            self.get_profile_folder()

        self.__sync_profile(self.profile_dir,self.profile)

    def quit_and_sync(self):
        # sync
        self.sync_data()
        # quit
        self.driver.quit()

    def quit(self):
        self.driver.quit()

    def safe_quit(self):
        try:
            self.driver.quit()
        except:
            print('an error occus when quit firefox')

    def wait_page_loaded(self):
        try:
            print('wait page load')
            WebDriverWait(self.driver, 60).until(
                lambda driver: driver.execute_script('return document.readyState') == 'complete'
            )
        except:
            pass
