from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep

class Zalo:
    def __init__(self,browser):
        self.browser = browser

    def send_sms(self, to, msg):
        self.browser.get('https://chat.zalo.me/')
        # contact-search-input
        search_i = WebDriverWait(self.browser,60).until(
            EC.element_to_be_clickable((By.ID, 'contact-search-input'))
        )
        sleep(0.1)
        for c in to:
            search_i.send_keys(c)
            sleep(0.1)
        sleep(0.5)
        result_p = WebDriverWait(self.browser,60).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '#searchResultList .item'))
        )
        result_p.click()
        sleep(1)
        input = WebDriverWait(self.browser,60).until(
            EC.element_to_be_clickable((By.ID, 'richInput'))
        )
        input.click()
        input.send_keys('', msg)
        sleep(1)
        input.send_keys(Keys.ENTER)

    def bulk_send_msg(self, to, lines):
        self.browser.get('https://chat.zalo.me/')
        # contact-search-input
        search_i = WebDriverWait(self.browser,60).until(
            EC.element_to_be_clickable((By.ID, 'contact-search-input'))
        )
        sleep(0.1)
        for c in to:
            search_i.send_keys(c)
            sleep(0.1)
        sleep(0.5)
        search_i.send_keys(Keys.BACKSPACE)
        sleep(0.5)
        search_i.send_keys(Keys.ENTER)
        sleep(0.6)
        # result_p = WebDriverWait(self.browser,60).until(
        #     EC.element_to_be_clickable((By.CSS_SELECTOR, '#searchResultList .item'))
        # )
        # result_p.click()
        sleep(1)
        input = WebDriverWait(self.browser,60).until(
            EC.element_to_be_clickable((By.ID, 'richInput'))
        )
        input.click()

        for msg in lines:
            input.send_keys('', msg)
            sleep(0.5)
            input.send_keys(Keys.ENTER)
            sleep(2)
            input = self.browser.find_element_by_id('richInput')
            input.click()
