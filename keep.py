import subprocess
import argparse

filename = 'main.py'

while True:
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--script", "-S", help="script")

    # Read arguments from the command line
    args = parser.parse_args()

    if args.script:
        filename = args.script

    p = subprocess.Popen('python %s' % (filename, ), shell=True).wait()

    if p != 0:
        continue
    else:
        break
