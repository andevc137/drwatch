import json
import requests
from time import sleep

class TwoCaptcha:
    def __init__(self, apikey):
        self.apikey = apikey
        self.endpoint = 'http://2captcha.com/'

    def create_task(self, website_url, website_key, task_type = 'userrecaptcha'):
        url = self.endpoint + 'in.php'
        data = {'key': self.apikey, 'method': task_type, 'pageurl': website_url, 'googlekey': website_key, 'json': 1}
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}

        res = requests.post(url, data=json.dumps(data), headers=headers)
        return res.json()['request']
    
    def get_answer(self, taskId):
        url = '%sres.php?key=%s&action=get&id=%s&json=1' % (self.endpoint,self.apikey,taskId)
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        rdata = None

        while True:
            res = requests.get(url, headers=headers)
            if res.status_code != 200:
                break

            rdata = res.json()

            if rdata['request'] != 'CAPCHA_NOT_READY':
                break

            sleep(5)

        return rdata

    def solve(self, website_url, website_key, task_type = 'userrecaptcha'):
        taskId = self.create_task(website_url, website_key, task_type)
        answer = self.get_answer(taskId)

        return answer['request']
