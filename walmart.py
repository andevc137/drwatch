from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from twocaptcha import TwoCaptcha
from time import sleep

class Walmart:
    def __init__(self, firefox):
        self.firefox = firefox
        self.browser = firefox.get_browser()

    def get_product_info(self,link,ck=True):
        self.firefox.get(link)
        self.firefox.wait_page_loaded()
        if ck:
            sleep(0.5)
            self.bypass_captcha()
        price = self.browser.execute_script('return Number(document.querySelector(".prod-PriceSection .price-group").innerText.replace(/[^0-9\.]/g,""));')
        out_of_stock = self.browser.execute_script('return document.querySelector(".prod-PriceSection").innerText.toLowerCase().indexOf("out of stock") !== -1;')
        return {'price': price, 'out_of_stock': out_of_stock}

    def is_bot_def(self):
        print('check captcha')
        try:
            botcheck = self.browser.execute_script('return document.querySelector(".prod-PriceSection .price-group") == null && (document.getElementById("bot-handling-challenge") !== null || location.href.indexOf("walmart.com/blocked") !== -1);')
            return botcheck
        except:
            pass

        return False

    def bypass_captcha(self):
        try:
            if self.is_bot_def():
                # solve captcha
                capt = TwoCaptcha('6d8db75d19834b645cd6d0a685c75bfb')
                answer = capt.solve('https://www.walmart.com/blocked', '6Lc8-RIaAAAAAPWSm2FVTyBg-Zkz2UjsWWfrkgYN')
                # fill
                print('fill captcha')
                self.browser.execute_script('document.getElementById("g-recaptcha-response").innerHTML="%s";' % answer)
                sleep(0.5)
                self.browser.execute_script('handleCaptcha("%s");' % answer)
                self.firefox.wait_page_loaded()
                sleep(1)
                WebDriverWait(self.browser,60).until(
                    EC.element_to_be_clickable((By.ID, 'hf-account-flyout'))
                )
        except:
            print('an error occus')
